# [terraform-project]/outputs.tf

# This will return a map with all of the outputs for the module
output "experiment_cluster" {
  value = module.experiment_cluster
}

# If you need a specific key as an output, you can use the dot notation shown above to access the map value.
output "experiment_cluster_name" {
  value = module.experiment_cluster.cluster.name
}
