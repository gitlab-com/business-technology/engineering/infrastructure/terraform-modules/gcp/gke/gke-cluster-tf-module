# [terraform-project]/variables.tf

# Required variables

variable "gcp_project" {
  type        = string
  description = "The GCP project ID (may be a alphanumeric slug) that the resources are deployed in. (Example: my-project-name)"
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. us-east1)"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region. (Example: us-east1-c)"
}

# Optional variables with default values

variable "gcp_vpc_network" {
  type        = string
  description = "The GCP VPC network that the resources will be deployed in. (Example: default)"
  default     = "default"
}
